extends Node

const INFRA_PORT : int = 12599

var peer:= ENetMultiplayerPeer.new()

func peer_connected(pid: int):
	Logger.event(["Peer connected: ", pid])

func peer_disconnected(pid: int):
	Logger.event(["Peer disconnected: ", pid])

func peer_packet(id: int, packet: PackedByteArray):
	prints("Packet recieved from", id, "Data:", packet)

func _ready():
	peer.connect(&"peer_connected", peer_connected)
	peer.connect(&"peer_disconnected", peer_disconnected)

	var result = peer.create_server(INFRA_PORT, 4095)

	peer.get_incoming_connections()

	prints("Creating server result:", result)

func _on_timer_timeout():
	# check for incoming packets
	peer.poll()

	# if new packets arrived, let's process them
	if peer.get_available_packet_count() > 0:
		var request = peer.get_var()

		# just in case
		if request != null:
			prints("Request:", request)

			# Each request is an array with a header [0] and payload [1].
			# The header determines what type of request it is so we check for known headers
			if request[0] == "create_account":
				Logger.event(["Recieved request to create account for user ", request[1]["username"]])
				var result = PlayerAccounts.create_player_account(request[1]["username"], request[1]["password_hash"], request[1]["password_salt"])
				var reply = ["create_account", result]
				peer.set_target_peer(request[1]["peer_id"])
				peer.put_var(reply)
			elif request[0] == "user_login":
				Logger.event(["Recieved request to login user ", request[1]["username_hash"]])
				var result = PlayerAccounts.player_login(request[1]["username_hash"])
				var reply = ["user_login", result]
				prints("Response:", reply)

				peer.set_target_peer(request[1]["peer_id"])
				peer.put_var(reply)
			elif request[0] == "user_auth":
				Logger.event(["Recieved request to authenticate user ", request[1]["username_hash"]])
				var result = PlayerAccounts.player_auth(request[1]["username_hash"], request[1]["password_hash"])
				var reply = ["user_auth", result]
				prints("Response:", reply)

				peer.set_target_peer(request[1]["peer_id"])
				peer.put_var(reply)
			elif request[0] == "user_update_avatar":
				var reply = ["user_update_avatar"]
				if PlayerAccounts.verify_token(request[1]["username_hash"], request[1]["token"]):
					print("Token verification passed")
					var data : PackedByteArray = request[2]["data"]
					var recieved_hash = request[2]["hash"]
					prints("Recieveed avatar image of size", data.size(),"bytes")

					var computed_hash = Storage.hash_data(data)

					if computed_hash == recieved_hash:
						print("Data integrity verified")
						PlayerAccounts.update_avatar(request[1]["username_hash"], recieved_hash, data)
						Logger.event(["Updated avatar for user ", request[1]["username_hash"]])
						reply.append(OK)
					else:
						print("Hash mismatch. Data integrity compromised.")
						reply.append(ERR_INVALID_DATA)
				else:
					print("Token verification failed")
					reply.append(ERR_UNAUTHORIZED)

				peer.set_target_peer(request[1]["peer_id"])
				peer.put_var(reply)
			elif request[0] == "retrieve_data":
				var reply = ["retrieve_data"]
				var hash = request[1]["hash"]
				if not(hash is PackedByteArray):
					hash = var_to_str(hash)
					if not(hash is PackedByteArray):
						print("Hash is not a PackedByteArray")

				var data = Storage.retrieve(request[1]["hash"], request[1]["username_hash"])

				if data is PackedByteArray:
					reply.append({"data" = data})
				else:
					reply.append({"error" = data})

				peer.set_target_peer(request[1]["peer_id"])
				peer.put_var(reply)
				Logger.event(["Retrieved storage unit ", request[1]["hash"], " for user " , request[1]["username_hash"]])
