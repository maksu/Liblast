extends Control

var main_scene : PackedScene


const main_scene_path := "res://Main.tscn"
const dedicated_scene_path := "res://Dedicated.tscn"

var scene_path := main_scene_path # default main scene

var is_dedicated := false

var duration : float
var expected_duration : float = 0

enum LoadingStage {LOADING, SPAWNING}

var stage := LoadingStage.LOADING

# Called when the node enters the scene tree for the first time.
func _ready():
	var file = FileAccess.open("user://boot.time", FileAccess.READ)
	if file:
		expected_duration = file.get_float()
#		if file.get_error() == OK:
#			prints("Read expected load time as", expected_duration)

	var args = OS.get_cmdline_args()

	for i in range(args.size()):
		match args[i]:
			"--dedicated-host":

				is_dedicated = true

				var map : String
				if args.size() > i:
					map = args[i+1]
				else:
					map = "MapA" # default map as fallback

				MultiplayerState.game_config.map = map
				MultiplayerState.start_server(Globals.MultiplayerRole.DEDICATED_SERVER)

				scene_path = dedicated_scene_path # overrriding main scene

	ResourceLoader.load_threaded_request(scene_path, "PackedScene", false)


func spawn_main_scene() -> void:
	var scene = ResourceLoader.load_threaded_get(scene_path)
	var main_scene = scene.instantiate()
	get_tree().root.add_child(main_scene)
	Settings.call_apply_var("display_fullscreen") # enable fullscreen if desired

	if expected_duration == 0:
		expected_duration = duration # if nothing was saved, use measired data
	else: # othwerise average loading time with existing data
		expected_duration = (duration + expected_duration) /2


	if not is_dedicated: # only store boot time for client
		var file = FileAccess.open("user://boot.time", FileAccess.WRITE)
		if file:
			file.store_float(expected_duration)
	#		if file.get_error() == OK:
	#			prints("Saved expected load time as", expected_duration)
			file.flush()

	# free the boot screen
	queue_free()


func _process(delta):
	duration = Time.get_ticks_msec()
#	prints(duration)
	$TextureProgressBar.value = duration / (expected_duration if expected_duration != 0 else 5.0)

	if stage == LoadingStage.LOADING:
		var progress = []
		ResourceLoader.load_threaded_get_status(scene_path, progress)

		if progress[0] == 1: # loading finished
			spawn_main_scene()
			stage = LoadingStage.SPAWNING
