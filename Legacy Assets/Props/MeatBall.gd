extends StaticBody3D

@onready var main = get_tree().root.get_node("Main")

var impact_player = preload("res://Assets/Effects/ImpactBlood.tscn")
var gibs_vfx = preload("res://Assets/Effects/Gibs.tscn")
var blood_decal = preload("res://Assets/Decals/Blood/BloodSplash.tscn")

var health = 1000
var dead = false

@rpc(call_local, any_peer, reliable) func take_damage(attacker: int, hit_position: Vector3, hit_normal: Vector3, damage:int, source_position: Vector3, damage_type, push: float) -> void:
	var attacker_node = main.get_node("Players").get_node(str(attacker))
	
	if is_multiplayer_authority() and not dead:
		print("Meatball has taken damage: ", damage, " by: ", attacker, " from: ", source_position)
		health -= damage # reduce health

		if health <= 0: # are we dead?
			health = 0 # Prevent minus values, especially to be shown to players
			
			rpc(&'die', attacker)

			if attacker == get_multiplayer_authority():
				print("Suicide")
			else:
				print("Died")
				if attacker_node:
					attacker_node.rpc(&'damage_feedback', true) # let the attacker know he's got a kill
		else:
			if attacker_node:
				attacker_node.rpc(&'damage_feedback', false) # let the attacker know he's got a hit

		if not attacker_node:
			print_debug("Attacker node not found, check attacker pid: ", attacker)

	if not dead:
		$PainSFX.play()

	# spawn the bullet hit effect
	var impact_vfx = impact_player.instantiate()
	impact_vfx.global_transform = impact_vfx.global_transform.looking_at(hit_normal)
	impact_vfx.global_transform.origin = hit_position
	get_tree().root.add_child(impact_vfx)
