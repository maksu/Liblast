extends VBoxContainer

var previous_menu: Node = null

@onready var menu_root = get_tree().root.get_node("Main").get_node("MenuRoot")
@onready var GUI = get_parent().get_parent()
@onready var Main = get_parent().get_parent().get_parent()

func _ready() -> void:
	if previous_menu == null:
		$Back.hide()

func open_menu(path: String) -> void:
	var menu = load(path).instantiate()
	menu.previous_menu = self
	get_parent().add_child(menu)
	hide()

func go_back() -> void:
	$Back/ClickSound.play()
	previous_menu.show()
	previous_menu = null
	queue_free()

func _on_Back_mouse_entered() -> void:
	$Back/HoverSound.play()
