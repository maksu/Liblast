extends "res://Assets/Menu/MenuData.gd"

func on_value_changed():
	print("Trying to set color button value to ", value)
	self.color = Color(value)

func _on_menu_color_color_changed(color):
	set_var(color.to_html())
