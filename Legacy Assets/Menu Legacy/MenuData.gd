extends "res://Assets/Menu/MenuItem.gd"

signal data_changed(data)

@export var var_name = ""

var value:
	set(_value):
		value = _value
		print("Setting value of menu element ", name, " to ", _value)
		on_value_changed()

func on_value_changed(): 
	pass

func value_changed_externally(_var_name, _value):
	if _var_name == var_name:
		if value != _value:
			value = _value
			on_value_changed()

func _ready():
	value = Settings.get_var(var_name)
	Settings.var_changed.connect(value_changed_externally)

func set_var(_value):
	value = _value
	Settings.set_var(StringName(var_name), _value)
