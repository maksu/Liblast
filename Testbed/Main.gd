extends Node3D

var peer = ENetMultiplayerPeer.new()
@onready var player_spawner = $CharactersSpawner

func _peer_connected(pid):
	print(pid," connected")

	# Only server spawns
	if multiplayer.is_server():
		player_spawner.spawn(pid)


func _on_host_pressed():

	peer.peer_connected.connect(_peer_connected)
	peer.create_server(9999)
	multiplayer.multiplayer_peer = peer

	# Spawns the server
	player_spawner.spawn(1)


func _on_join_pressed():
	
	peer.peer_connected.connect(_peer_connected)
	peer.create_client("localhost", 9999)
	multiplayer.multiplayer_peer = peer
