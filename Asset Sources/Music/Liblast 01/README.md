# unfa - Liblast 01 music track

This track was created with Ardour 7.2/7.3 on Arch Linux in February 2023.

## Plugins used in this session:

CNT | TYPE | NAME
 12 * LV2    Surge XT (by Surge Synth Team)
  8 * LV2    Geonkick Single (by Iurie Nistor)
  8 * LV2    ACE Compressor (stereo) (by Ardour Community)
  8 * Lua    ACE High/Low Pass Filter (by Ardour Community)
  6 * VST3   Diopser (by Robbert van der Helm)
  5 * LV2    ZaMultiCompX2 (by Damien Zammit)
  5 * LV2    x42-eq - Parametric Equalizer Stereo (by Robin Gareus)
  5 * LV2    CHOWTapeModel (by chowdsp)
  4 * VST3   Spectral Compressor (by Robbert van der Helm)
  4 * VST2   Verbity2 (by airwindows)
  3 * VST2   Galactic (by airwindows)
  3 * LV2    x42-dpl - Digital Peak Limiter Stereo (by Robin Gareus)
  3 * LV2    Calf Stereo Tools (by Calf Studio Gear)
  2 * VST2   DeBess (by airwindows)
  2 * LV2    Linear Predictive Coding (by knecraft)
  2 * LV2    Dragonfly Early Reflections (by Michael Willis)
  2 * LV2    Aether (by Dougal Stewart)
  2 * LADSPA DC Offset Remover (by Steve Harris)
  1 * VST2   Pyewacket (by airwindows)
  1 * VST2   MatrixVerb (by airwindows)
  1 * VST2   ChorusEnsemble (by airwindows)
  1 * VST2   Chorus (by airwindows)
  1 * VST2   BrightAmbience3 (by airwindows)
  1 * LV2    Tal-Dub-3 (by TAL-Togu Audio Line)
  1 * LV2    Spectacle (by JPC)
  1 * LV2    MaPitchshift (by DISTRHO)
  1 * LV2    Calf Multi Chorus (by Calf Studio Gear)
  1 * LV2    B.Oops (by Sven Jaehnichen)
  1 * LV2    B.Choppr (by Sven Jaehnichen)
  1 * LV2    ACE Expander (stereo) (by Ardour Community)
  1 * LADSPA Hard Gate (by CMT)
