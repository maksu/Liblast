# Asset Workflow

The models (high poly and lowpoly) are in the PlasmaGun.blend Blender project.

In this project I used TexTools add-on to assing Material IDs to different parts on the highpoly model, and then baked these IDs to a Base Color map using Bysteds Blender Baker.
World/Object Space normals have to be added as a second Normal pass with changed space (tangent to object). BBB breaks the preview once yo uhave 2 normap passes as it will always show the 2nd map, even if you're baking to the frist one.

A long-standing sisue with BBB is that normal maps break if you try to bake them for all objects at once. So you can bake Color/ID, AO and world space normals (I think) but Tangent Space normals will only bake proeptrly if you select each object and bake that pass for it one by one. I know. That sucks. I've created a [fork](https://codeberg.org/unfa/BystedsBlenderBakerPlus) of BBB to fix another issue (with bakes always being 8-bit unless you manually make the bake_image buffer 32-bit Float), so I'll probably try to fix more over time (original author seems to not be working on it any more). Help is welcome!

All the baked maps are then processed in Material maker to produce a final PBR material.

I am no sure if it's better to commit Blender projects compressed or not. With uncomressed I assume Git LFS can do soem deduplication and reduce used file size, but it's possible that won't work anyway.
Feel free to enlighten me about this.

To export the GLTF fro Godot, you need to make sure all meshes use the same material and then select the exproter to include materials (skip the images though!) - using placeholder materials won't work, and you'll get one material per mesh which is not goign to work for manually remapping these in Godot.

Use these settings and there's going to be one material to worry about in Godot:

```
- Material
	- Materials: Export
	- Images: None
```

-- unfa 2023-02-11
